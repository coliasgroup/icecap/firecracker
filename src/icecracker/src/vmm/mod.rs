use std::ops::Range;
use std::os::unix::io::{AsRawFd, RawFd};
use std::os::unix::thread::JoinHandleExt;
use std::sync::{Arc, Barrier, Mutex};

use devices::{
    virtio::{net::Net, MmioTransport},
    Bus,
};
use event_manager::{
    EventManager as BaseEventManager, EventOps, Events, MutEventSubscriber, SubscriberOps,
};
use logger::{debug, error, info, trace, warn};
use rate_limiter::BucketUpdate;
use utils::epoll::EventSet;
use utils::eventfd::EventFd;
use utils::net::mac::MacAddr;
use vm_memory::{
    Address, GuestAddress, GuestMemory, GuestMemoryMmap, GuestRegionMmap, MmapRegionBuilder,
};

use icecap_host_user::*;

use crate::error::Result;

pub type EventManager = BaseEventManager<Arc<Mutex<dyn MutEventSubscriber>>>;

const MMIO_LEN: u64 = 0x1000;
const MMIO_ADDR: u64 = 0; // TODO

const VCPU_CPU: usize = 1;
const EVENT_MANAGER_CPU: usize = 2;

const REALM_ID: usize = 0;

const REALM_MEMORY_REGION_START: usize = 0x18000200000;
const REALM_MEMORY_REGION_SIZE: usize = 2 << 21;
const REALM_MEMORY_REGION_DEVICE: &str = "/dev/icecap_net0";

const VIRTUAL_NODE: usize = 0;

const NET_ID: &str = "foo_net";
const TAP_IF_NAME: &str = "veth0";
const GUEST_MAC: &str = "00:01:56:78:9a:bc";

pub fn run() -> Result<()> {
    let handle = spawn_on_cpu(format!("ic_evt_manager"), EVENT_MANAGER_CPU, move || {
        run_inner().unwrap()
    })?;
    handle.join().unwrap();

    Ok(())
}

fn run_inner() -> Result<()> {
    let mut event_manager = EventManager::new().unwrap();
    let guest_memory = mk_guest_memory()?;
    let emulation = Emulation::new(&mut event_manager, &guest_memory)?;
    let emulation = Arc::new(Mutex::new(emulation));

    let virtual_node = VIRTUAL_NODE;

    let vcpu_thread_handle = spawn_on_cpu(format!("ic_vcpu"), VCPU_CPU, {
        let emulation = emulation.clone();
        move || {
            run_vcpu(emulation, virtual_node).unwrap();
        }
    })?;

    loop {
        event_manager.run().unwrap();
    }

    Ok(())
}

fn run_vcpu(emulation: Arc<Mutex<Emulation>>, virtual_node: usize) -> Result<()> {
    let realm_id = REALM_ID;

    let mut host = Host::new().unwrap();

    let mut response = None;
    loop {
        debug!("run with response={:x?}", response);
        let condition = host
            .run_realm_node(realm_id, virtual_node, response)
            .unwrap();
        debug!("return with condition={:x?}", condition);
        match condition {
            RunRealmReturnCondition::RealmRequest(req) => {
                let tag = req.0;
                match tag {
                    REQUEST_TAG_READ => {
                        let addr = req.1;
                        let value = emulation.lock().unwrap().read(addr)?;
                        response = Some(value as u64);
                    }
                    REQUEST_TAG_WRITE => {
                        let addr = req.1;
                        let value = req.2 as u32;
                        emulation.lock().unwrap().write(addr, value)?;
                        response = Some(0);
                    }
                    _ => {
                        panic!();
                    }
                }
            }
            RunRealmReturnCondition::Pass {
                must_repeat_response,
            } => {
                if !must_repeat_response {
                    response = None;
                }
            }
        }
    }
}

fn spawn_on_cpu<F, T>(name: String, cpu: usize, f: F) -> Result<std::thread::JoinHandle<T>>
where
    F: FnOnce() -> T,
    F: Send + 'static,
    T: Send + 'static,
{
    let barrier = Arc::new(Barrier::new(2));

    let handle = std::thread::Builder::new().name(name).spawn({
        let barrier = barrier.clone();
        move || {
            barrier.wait();
            f()
        }
    })?;

    unsafe {
        let mut set: libc::cpu_set_t = std::mem::zeroed();
        libc::CPU_SET(cpu, &mut set);
        let pthread = handle.as_pthread_t() as *mut std::ffi::c_void; // HACK for musl
        let ret =
            libc::pthread_setaffinity_np(pthread, std::mem::size_of::<libc::cpu_set_t>(), &set);
        assert_eq!(ret, 0);
    }

    barrier.wait();

    Ok(handle)
}

fn mk_guest_memory() -> Result<GuestMemoryMmap> {
    let guest_addr = GuestAddress::new(REALM_MEMORY_REGION_START as u64);
    let size = REALM_MEMORY_REGION_SIZE;

    let prot = libc::PROT_READ | libc::PROT_WRITE;
    let flags = libc::MAP_NORESERVE | libc::MAP_SHARED;
    let file = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .open(REALM_MEMORY_REGION_DEVICE)
        .unwrap();
    let fd = file.as_raw_fd();
    let offset = 0;

    let host_addr = unsafe {
        libc::mmap(
            std::ptr::null_mut(),
            size,
            prot,
            flags,
            fd,
            offset as libc::off_t,
        )
    };

    assert_ne!(host_addr, libc::MAP_FAILED);

    // TODO
    // NOTE this is the default in src/vmm/src/vmm_config/machine_config.rs
    let track_dirty_pages = false;

    let bitmap = match track_dirty_pages {
        true => todo!(),
        false => None,
    };

    let region = unsafe {
        MmapRegionBuilder::new_with_bitmap(size, bitmap)
            .with_raw_mmap_pointer(host_addr as *mut u8)
            .with_mmap_prot(prot)
            .with_mmap_flags(flags)
            .build()
            .unwrap()
    };

    let guest_region = GuestRegionMmap::new(region, guest_addr)?;
    let guest_regions = vec![guest_region];
    let guest_memory = GuestMemoryMmap::from_regions(guest_regions)?;

    Ok(guest_memory)
}

struct Emulation {
    bus: Bus,
    net_queue_evts: Vec<EventFd>,
}

impl Emulation {
    fn new(event_manager: &mut EventManager, guest_memory: &GuestMemoryMmap) -> Result<Self> {
        let mut bus = Bus::new();

        let (net_queue_evts, interrupt_evt) =
            Self::create_net_device(event_manager, guest_memory, &mut bus)?;

        event_manager.add_subscriber(Arc::new(Mutex::new(InterruptSubscriber::new(interrupt_evt))));

        Ok(Self {
            bus,
            net_queue_evts,
        })
    }

    fn create_net_device(
        event_manager: &mut EventManager,
        guest_memory: &GuestMemoryMmap,
        bus: &mut Bus,
    ) -> Result<(Vec<EventFd>, EventFd)> {
        let addr = MMIO_ADDR;
        let len = MMIO_LEN;

        let id = NET_ID.to_string();
        let tap_if_name = TAP_IF_NAME.to_string();
        let guest_mac = MacAddr::parse_str(GUEST_MAC).unwrap();
        let guest_mac = Some(&guest_mac);
        let rx_rate_limiter = Default::default();
        let tx_rate_limiter = Default::default();
        let allow_mmds_requests = false;

        let net_device = Net::new_with_tap(
            id,
            tap_if_name,
            guest_mac,
            rx_rate_limiter,
            tx_rate_limiter,
            allow_mmds_requests,
        )
        .unwrap();

        let net_device = Arc::new(Mutex::new(net_device));

        event_manager.add_subscriber(net_device.clone());

        let mmio_device = MmioTransport::new(guest_memory.clone(), net_device);

        let (queue_evts, interrupt_evt) = {
            let locked_device = mmio_device.locked_device();
            let mut queue_evts = vec![];
            for (i, queue_evt) in locked_device.queue_events().iter().enumerate() {
                queue_evts.push(queue_evt.try_clone().unwrap());
            }
            (
                queue_evts,
                locked_device.interrupt_evt().try_clone().unwrap(),
            )
        };

        bus.insert(Arc::new(Mutex::new(mmio_device)), addr, len)
            .unwrap();

        Ok((queue_evts, interrupt_evt))
    }

    fn read(&mut self, addr: u64) -> Result<u32> {
        let mut data = [0; std::mem::size_of::<u32>()];
        let success = self.bus.read(addr, &mut data);
        assert!(success);
        let value = u32::from_le_bytes(data);
        Ok(value)
    }

    fn write(&mut self, addr: u64, value: u32) -> Result<()> {
        if addr == u64::from(devices::virtio::NOTIFY_REG_OFFSET) {
            let index = value as usize;
            debug!("virtio net notify {}", index);
            self.net_queue_evts[index].write(1)?;
        } else {
            let data = value.to_le_bytes();
            let success = self.bus.write(addr, &data);
            assert!(success);
        }
        Ok(())
    }
}

struct InterruptSubscriber {
    interrupt_evt: EventFd,
    count: usize,
}

impl InterruptSubscriber {

    fn new(interrupt_evt: EventFd) -> Self {
        Self {
            interrupt_evt,
            count: 0,
        }
    }

    fn process_interrupt_evt(&mut self) {
        let host = Host::new().unwrap();
        host.signal_realm_net(0).unwrap();
        self.count += 1;
        if self.count % 1000 == 0 {
            info!("process_interrupt_evt: count = {}", self.count);
        }
    }
}

impl MutEventSubscriber for InterruptSubscriber {
    fn process(&mut self, event: Events, ops: &mut EventOps) {
        let source = event.fd();
        let event_set = event.event_set();

        let supported_events = EventSet::IN;
        if !supported_events.contains(event_set) {
            warn!(
                "Received unknown event: {:?} from source: {:?}",
                event_set, source
            );
            return;
        }

        let interrupt_evt_fd = self.interrupt_evt.as_raw_fd();

        match source {
            _ if source == interrupt_evt_fd => {
                self.interrupt_evt.read().unwrap();
                self.process_interrupt_evt();
            }
            _ => {
                warn!("Spurious event received: {:?}", source);
            }
        }
    }

    fn init(&mut self, ops: &mut EventOps) {
        ops.add(Events::new(&self.interrupt_evt, EventSet::IN))
            .unwrap()
    }
}
