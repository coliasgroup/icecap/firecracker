#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unreachable_code)]

use logger::{debug, error, info, trace, warn};

use error::Result;

mod error;
mod vmm;

const LOG_LEVEL: logger::LevelFilter =
    // logger::LevelFilter::Trace;
    // logger::LevelFilter::Debug;
    logger::LevelFilter::Info;
    // logger::LevelFilter::Warn;

const INSTANCE_ID: &str = "x";

fn main() -> Result<()> {
    logger::set_max_level(LOG_LEVEL);
    logger::LOGGER.set_instance_id(INSTANCE_ID.to_string());

    info!("icecracker enter");

    vmm::run()?;

    info!("icecracker exit");

    Ok(())
}
